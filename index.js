const Influx = require("influxdb-nodejs");
const fetch = require("node-fetch");

const client = new Influx(process.env.INFLUXDB_URI);

const fieldSchema = {
  count: "i"
};

const request = fetch(
  `https://sentry.io/api/0/projects/${process.env.ORGANIZATION}/${
    process.env.PROJECT
  }/stats/`,
  {
    headers: {
      Authorization: `Bearer ${process.env.TOKEN}`
    }
  }
);
const database = client.createDatabase();
Promise.all([request, database])
  .then(([data]) => data)
  .then(async data => {
    const list = await data.json();
    list.forEach(([timestamp, count]) => {
      client
        .write(process.env.PROJECT)
        .field({
          count
        })
        .time(timestamp * 1000, "ms")
        .queue();
    });
    await client.syncWrite();
  })
  .catch(err => {
    console.error("ERROR", err);
  });
