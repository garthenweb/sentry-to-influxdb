# sentry-to-influxdb

This project exports the counts of a project from sentry and saves this to a influxdb database.

Its purpose it to run this script on a regular basis to fill the database to show statistics in e.g. [Grafana](https://grafana.com/).

## Execute

Following env variables need to be passed:

- `TOKEN`: Auth token to access the sentry organization. You can generate yours [in the sentry account settings](https://sentry.io/settings/account/api/auth-tokens/). Only ´project:read´ access is required.
- `INFLUXDB_URI`: The url to the influxdb
- `ORGANIZATION`: The sentry organization name stats should be read from
- `PROJECT`: The sentry project name the stats should be read from

### Docker

```bash
docker run -e "TOKEN=MY-AUTH-TOKEN" -e "INFLUXDB_URI=http://url-to-influxdb:8086/dbname" -e "ORGANIZATION=my-organization" -e "PROJECT=my-project" registry.gitlab.com/garthenweb/sentry-to-influxdb:latest
```

### node

```bash
npm install
TOKEN=MY-AUTH-TOKEN INFLUXDB_URI=http://url-to-influxdb:8086/dbname ORGANIZATION=my-organization PROJECT=my-project node index.js
```

## Grafana

![Screenshot Grafana Query](https://gitlab.com/garthenweb/sentry-to-influxdb/raw/master/influxdb-query.jpg "Screenshot Grafana Query")

## License

Licensed under the [MIT License](https://opensource.org/licenses/mit-license.php).
