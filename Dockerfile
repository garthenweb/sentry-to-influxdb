FROM node:10-alpine as builder

WORKDIR /build

COPY package.json package.json
COPY package-lock.json package-lock.json

RUN npm ci

FROM node:10-alpine

WORKDIR /app

COPY index.js index.js
COPY --from=builder /build/node_modules node_modules

CMD [ "node", "index.js"]
